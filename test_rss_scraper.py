import os
import sys
import glob
import feedparser
from dateutil import parser
import datetime
import json
from load_rss_urls_from_txt import load_rss_urls_from_txt
from load_rss_urls_from_opml import load_rss_urls_from_opml

output_file_path = r'/home/pi/tmp'
urls_file_path = r'/home/pi/newsbot/urls'
remote_storage_path = r'remote:/newsbot'

#
p = os.path.sep.join([urls_file_path, '**', '*.txt'])
file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]
file_list = [r'/home/pi/newsbot/urls/newletters.txt']
urls = {}
urls.update(load_rss_urls_from_txt(file_list))
# urls.update(load_rss_urls_from_opml(urls_file_path))
                
print(f"{len(urls)} unique urls loaded")

def extract_date_from_entry(entry) -> str:
    date = None
    if hasattr(entry, 'updated'):
        try:
            date = parser.parse(entry.updated)
        except:
            pass
    if hasattr(entry, 'published'):
        try:
            date = parser.parse(entry.published)
        except:
            pass
    if hasattr(entry, 'prism_publicationdate'):
        try:
            date = parser.parse(entry.prism_publicationdate)
        except:
            pass
    if hasattr(entry, 'datetime'):
        try:
            date = parser.parse(entry.datetime)
        except:
            pass
    if hasattr(entry, 'time'):
        try:
            date = parser.parse(entry.time['datetime'])
        except:
            pass        
    return date


tod = datetime.datetime.now()
ref_date = tod - datetime.timedelta(days = 1)

# get feeds and entries from urls
results = []
for channel, url in urls.items():
    try:
        feed = feedparser.parse(url)
    except Exception as e:
        print(str(e))
    feed_title = feed.feed.title if hasattr(feed.feed, 'title') else url
    print(f'Processing {feed_title}')
    for entry in feed.entries:
        print(entry)
        title = entry.title
        link = entry.link
        summary = entry.summary if hasattr(entry, 'summary') else None
        date = extract_date_from_entry(entry)
        if date is None or (date.timestamp() < ref_date.timestamp()):
            break
        entry_dict = {'feed': feed_title, 'timestamp': date.timestamp(), 'title': title, 'summary': summary, 'link': link}
        results.append(entry_dict)

# json_file_name = os.path.sep.join([output_file_path, f"rss_{datetime.datetime.now().strftime('%y%m%d_%H%M%S')}.json"])
# with open(json_file_name, 'w', encoding='utf-8') as fp:
#     json.dump(results, fp, indent=4, sort_keys=True, default=str)
# 

    
