import os
import sys
import glob
import datetime

# run this from cron, e.g.
#  0 0 * * 0 /usr/bin/python /home/pi/newsbot/clean_temp_folder.py

past_nr_of_days = 10

output_file_path = r'/home/pi/tmp'
remote_storage_path = r'remote:/newsbot'

p = os.path.sep.join([output_file_path, '**', '*.*'])
file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]

now = datetime.datetime.now()
for file_name in file_list:
    if datetime.datetime.fromtimestamp(os.path.getmtime(file_name)) < now - datetime.timedelta(days=past_nr_of_days):
        os.remove(file_name)
    if file_name.endswith('.log'):
        os.remove(file_name)
