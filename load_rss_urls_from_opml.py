import os
import glob
import re
from bs4 import BeautifulSoup

# import urls from various opml files
def load_rss_urls_from_opml(urls_folder) -> list:
    urls = {}
    p = os.path.sep.join([urls_folder, '**', '*.opml'])
    file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]
    for i, file_path in enumerate(file_list):
        with open(file_path, mode ='r') as file:
            soup = BeautifulSoup(file, 'xml')
            outlines = soup.find_all('outline')
            for outline in outlines:
                if outline != '\n':
                    if 'title' in outline.attrs and 'xmlUrl' in outline.attrs:
                        channel = outline['title']
                        url = outline['xmlUrl']
    ##                    basename = os.path.basename(file_path).split('.')[0].split('-')[0]
    ##                    channel = basename + ' - ' + outline['title']                    
                        if url not in urls.values():
    ##                        print(channel, url)
                            urls[channel] = url
    return urls
