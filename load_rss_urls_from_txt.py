import re

# import urls from various txt files
def load_rss_urls_from_txt(file_list) -> list:
    urls = {}
    for i, file_path in enumerate(file_list):
        with open(file_path, mode ='r') as file:
            for line in file:
                if line == '\n':
                    continue
                line = re.sub('"', '', line)
                channel, url = line.split(',')
                if line[0] == '#': # in channel:
                    break
    ##            basename = os.path.basename(file_path).split('.')[0]
    ##            channel = basename + ' - ' + channel
                url = re.sub(' ', '', url)
                url = re.sub('\n', '', url)
                if url not in urls.values():
    ##                print(channel, url)
                    urls[channel] = url
    return urls
