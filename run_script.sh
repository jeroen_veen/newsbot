#!/bin/bash

# Path to your virtual environment
VENV_PATH="/home/pi/newsbot"

# Activate the virtual environment
source "$VENV_PATH/bin/activate"

# Execute your Python script
python /home/pi/newsbot/rss_feed_scraper.py

# Deactivate the virtual environment
deactivate
