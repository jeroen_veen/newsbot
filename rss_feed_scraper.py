import os
import sys
import glob
import feedparser
from dateutil import parser
import datetime
import json
# import mariadb
import subprocess
from load_rss_urls_from_txt import load_rss_urls_from_txt
from load_rss_urls_from_opml import load_rss_urls_from_opml

# run this from cron, e.g. daily at midnight, e.g.
#  0 0 * * * /usr/bin/python /home/pi/newsbot/rss_feed_scraper.py >> /home/pi/tmp/myjob.log 2>&1
#  0 0 * * * /home/pi/newsbot/run_script.sh >> /home/pi/tmp/rssFeedScraper.py.log 2>&1
# With a runscript like this:
# #!/bin/bash

# # Path to your virtual environment
# VENV_PATH="/home/pi/newsbot"

# # Activate the virtual environment
# source "$VENV_PATH/bin/activate"

# # Execute your Python script
# python /home/pi/newsbot/rss_feed_scraper.py

# # Deactivate the virtual environment
# deactivate

OUTPUT_PATH = r'/home/pi/tmp/newsbot'
urls_file_path = r'/home/pi/newsbot/urls'
REMOTE_STORAGE_PATH = r'remote:/newsbot'

#
p = os.path.sep.join([urls_file_path, '**', '*.txt'])
file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]
urls = {}
urls.update(load_rss_urls_from_txt(file_list))
urls.update(load_rss_urls_from_opml(urls_file_path))

print(f"{len(urls)} unique urls loaded")

def extract_date_from_entry(entry) -> str:
    date = None
    if hasattr(entry, 'updated'):
        try:
            date = parser.parse(entry.updated)
        except:
            pass
    if hasattr(entry, 'published'):
        try:
            date = parser.parse(entry.published)
        except:
            pass
    if hasattr(entry, 'prism_publicationdate'):
        try:
            date = parser.parse(entry.prism_publicationdate)
        except:
            pass
    if hasattr(entry, 'datetime'):
        try:
            date = parser.parse(entry.datetime)
        except:
            pass
    if hasattr(entry, 'time'):
        try:
            date = parser.parse(entry.time['datetime'])
        except:
            pass        
    return date


tod = datetime.datetime.now()
ref_date = tod - datetime.timedelta(days = 1)

# get feeds and entries from urls
results = []
for channel, url in urls.items():
    try:
        feed = feedparser.parse(url)
    except Exception as e:
        print(str(e))
    feed_title = feed.feed.title if hasattr(feed.feed, 'title') else url
    print(f'Processing {feed_title}')
    for entry in feed.entries:
        title = entry.title
        link = entry.link
        summary = entry.summary if hasattr(entry, 'summary') else None
        date = extract_date_from_entry(entry)
        if date is None or (date.timestamp() < ref_date.timestamp()):
            break
        entry_dict = {'feed': feed_title, 'timestamp': date.timestamp(), 'title': title, 'summary': summary, 'link': link}
        results.append(entry_dict)

try:
    if not os.path.exists(OUTPUT_PATH):
        os.makedirs(OUTPUT_PATH)
except OSError as e:
    print(f"Error creating folder: {e}")

json_file_name = os.path.sep.join([OUTPUT_PATH, f"rss_{datetime.datetime.now().strftime('%y%m%d_%H%M%S')}.json"])
with open(json_file_name, 'w', encoding='utf-8') as fp:
    json.dump(results, fp, indent=4, sort_keys=True, default=str)

if REMOTE_STORAGE_PATH is not None:
    try:
        subprocess.run(['rclone', 'copy', OUTPUT_PATH, REMOTE_STORAGE_PATH], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except Exception as e:
        print(str(e))


# # store news in database
# # The database server should automatically start at boot on your Raspberry Pi.
# #  If it doesn’t you can ensure this by enabling the service.
# # sudo systemctl enable mariadb
# 
# try:
#     conn = mariadb.connect(
#         user="pi",
#         password="pi",
#         host="localhost",
#         database="news"
#     )
# except mariadb.Error as e:
#     print(f"Error connecting to MariaDB Platform: {e}")
#     sys.exit(1)
# 
# cursor = conn.cursor()
# varlist = ['FEED', 'DATE', 'TITLE', 'SUMMARY', 'LINK']
# var_string = ', '.join('?' * len(varlist))
# query_string = 'INSERT INTO table VALUES (%s);' % var_string
# print(query_string)
# cursor.execute(query_string, varlist)


    
