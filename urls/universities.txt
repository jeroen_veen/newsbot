Leiden, https://www.universiteitleiden.nl/nieuws/rss.xml
MIT, https://www.technologyreview.com/feed/
MIT latest, https://news.mit.edu/rss/feed
MIT research, https://news.mit.edu/rss/research
MIT engineering, https://news.mit.edu/rss/school/engineering
MIT humanities, https://news.mit.edu/rss/school/humanities-arts-and-social-sciences
TUDelft, https://www.tudelft.nl/actueel/laatste-nieuws?tx_lookupfeed_feed%5Baction%5D=rss&tx_lookupfeed_feed%5Bcontroller%5D=Feed&tx_lookupfeed_feed%5Blimit%5D=15&tx_lookupfeed_feed%5BlookupUid%5D=60915&type=1657271090&cHash=f237cf30dc281641b653dcbf2a8363c9
UTwente, https://www.utwente.nl/nieuws.rss
TUe, https://www.tue.nl/en/research?type=150
TUe AI, https://www.tue.nl/en/research/institutes/eindhoven-artificial-intelligence-systems-institute?type=150
UU, https://www.uu.nl/nieuws.rss
UvA, https://www.uva.nl/onderzoek/onderzoek-aan-de-uva/onderzoeksnieuws/onderzoeksnieuws.rss
WUR, https://www.wur.nl/nl/Resources/RSS.htm
RUG, https://www.rug.nl/about-ug/latest-news/news/latest-rug-news!rss
smithsonian insider, https://insider.si.edu/feed/
smithsonian latest articles, https://www.smithsonianmag.com/rss/latest_articles/
smithsonian news, https://www.smithsonianmag.com/rss/smart-news/
smithsonian innovation, https://www.smithsonianmag.com/rss/innovation/
smithsonian arts-culture, https://www.smithsonianmag.com/rss/arts-culture/


