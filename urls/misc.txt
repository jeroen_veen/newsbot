Frauenhofer, https://www.iis.fraunhofer.de/en/rss/press.rss
Lancet, https://www.thelancet.com/rssfeed/lancet_online.xml
Lancet global, https://www.thelancet.com/rssfeed/langlo_online.xml
Lancet digital, https://www.thelancet.com/rssfeed/landig_current.xml
Lancet public, https://www.thelancet.com/rssfeed/lanpub_online.xml
IEEE spectrum, https://spectrum.ieee.org/
Oostnl nieuws, https://oostnl.nl/nl/rss/nieuws
Oostnl events, https://oostnl.nl/nl/rss/events
Eurostat news releases, https://ec.europa.eu/eurostat/en/search?p_p_id=estatsearchportlet_WAR_estatsearchportlet&p_p_lifecycle=2&p_p_state=maximized&p_p_resource_id=atom&_estatsearchportlet_WAR_estatsearchportlet_collection=CAT_PREREL
Eurostat news, https://ec.europa.eu/eurostat/en/search?p_p_id=estatsearchportlet_WAR_estatsearchportlet&p_p_lifecycle=2&p_p_state=maximized&p_p_resource_id=atom&_estatsearchportlet_WAR_estatsearchportlet_collection=CAT_EURNEW
Eurostat datasets, https://ec.europa.eu/eurostat/en/search?p_p_id=estatsearchportlet_WAR_estatsearchportlet&p_p_lifecycle=2&p_p_state=maximized&p_p_resource_id=atom&_estatsearchportlet_WAR_estatsearchportlet_collection=dataset
RIVM, https://www.rivm.nl/nieuws/rss.xml
BioSpace, https://app.newsloth.com/biospace-com/VlRSVw.rss
hackaday, https://hackaday.com/blog/feed/
techradar, https://www.techradar.com/rss
ocean noise blog, http://feeds.feedburner.com/OceanNoise






