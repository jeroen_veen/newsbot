import os
import sys
import glob
import datetime
import json


input_file_path = '/home/pi/tmp'

p = os.path.sep.join([input_file_path, '**', '*.json'])
file_list = [f for f in glob.iglob(p, recursive=True) if (os.path.isfile(f))]

for i, file_path in enumerate(file_list):
    with open(file_path, mode ='r') as file:
        data = json.load(file)